	drawMap("mapContainer", 'usa', dataJson, arcsJson);
	
	function drawMap(mapContainerID, scope, bubblesData, arcsData)	{
		/** function to structure the arcs array **/
		var arcsFun = function(){
			var arcs = [], len = arcsData.length;
			for(var i = 0; i < len; i++){
				arcs.push({destination:{latitude:arcsData[i].destlatitude,longitude:arcsData[i].destlongitude}, 
				origin:{latitude:arcsData[i].originlatitude,longitude:arcsData[i].originlongitude}});
			}
			return arcs;
		};
		
		var arcsObj = arcsFun();
		
		/** on windows resizing**/
		window.addEventListener('resize', function() {
				bubble_map.resize();
			});
		
		/** map initializing**/
		var bubble_map = new Datamap({
			element: document.getElementById(mapContainerID),
			scope: scope,
			responsive: true,
			geographyConfig: {
				popupOnHover: false,
				highlightOnHover: false
			},
			fills: {
				defaultFill: '#D1D1D1',
				'blue': '#2980E3',
				'verySmall': '#00E56F',
				'small':'#39AD5C',
				'medium':'#737549',
				'large':'#AC3D36',
				'veryLarge':'#E60523'
			}
		});
		
			/**for the hover event **/
		bubble_map.bubbles(bubblesData, {
			popupTemplate: function(geo, data) {
				var airportArcs = []; 
				arcsObj.forEach(function (arc){
					if(arc.origin.latitude == geo.latitude & arc.origin.longitude == geo.longitude){
						airportArcs.push(arc);
					}
				});
				return ['<div class="hoverinfo">Airport : ' + data.fullname +'<br/>Avg. Departing Flights Per Day: ' + 
				data.depperday +'<br/>Avg. Taxi In Time :' + data.taxiin + ' min<br/>Avg. Taxi Out Time :' + data.taxiout + 
				' min<br/>Avg. Delay :' + data.delay + ' min<br/>Avg. Cancellations Per day : '+ data.cancellations + '</div>' ,
				bubble_map.arc(airportArcs, {strokeWidth: 0.8, arcSharpness: 1.6, strokeColor: '#E32942'})]
			},
			borderColor: '#1d5491'
		});
		
		/**for states labels **/
		bubble_map.labels({labelColor: '#64748b', fontSize: 12});
		//bubble_map.legend();
		
		/** for the drilldown dashoard **/
		/*var year = $('#hiddenYear').val();
		var month = $('#hiddenMonth').val();
		bubble_map.svg.selectAll('.datamaps-bubble').on('click', function(geo) {
		SubmitForm('rdPage.aspx?rdReport=AirlineDD&Origin='+geo.name+'&month='+month+'&year='+year ,'',false,'',true,null);
		});*/
		
		/** initilaize arc for the 1th element **/
		var elem = bubblesData[0];
		var airportArcs = []; 
		arcsObj.forEach(function (arc){
		if(arc.origin.latitude == elem.latitude & arc.origin.longitude == elem.longitude){
		airportArcs.push(arc);
		}
		});
		bubble_map.arc(airportArcs, {strokeWidth: 0.8, arcSharpness: 1.6, strokeColor: '#E32942'});
		
	}	
		
		
