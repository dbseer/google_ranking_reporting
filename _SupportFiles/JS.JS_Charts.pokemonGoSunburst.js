drawChart(pokSeqJson);

function drawChart(data){
		
	// Dimensions of sunburst.
	var CContainer = $('#chartContainer');
	var width = CContainer.width();
	var height = CContainer.width();
	var radius = Math.min(width, height) / 2.5;    
	
	// Breadcrumb dimensions: width, height, spacing, width of tip/tail.
	var b = { w: 125, h: 30, s: 3, t: 10 };
	
	var initialScaleData = data;
	var pokeNameColor = {};
	var linearScale = d3.scale.linear()
					.domain([20,300])
					.range(["#FFFF66","red"]);
	
	for (var i = 0; i < initialScaleData.length; i++) {
	pokeNameColor[initialScaleData[i].pokeName] = linearScale(initialScaleData[i].stamina);
	};
	
	var pokeTypeColor = {	
		"FAIRY":"rgb(231, 150, 156)", "FLYING":"#b3de69", "GROUND":"#8B4513", 
		"POISON":"#808000", "NONE":"#bc80bd","WATER":"#87CEEB",
		"FIGHTING":"#B22222","STEEL":"rgb(224, 223, 219)",
		"ROCK":"#808487","PSYCHIC":"#fb8072","ICE":"rgb(223, 244, 255)",
		"GRASS":"#00FF00","FIRE":"#E25822","BUG":"#00BC35","ELECTRIC":"#4169E1",
		"DRAGON":"rgb(255, 237, 111)","NORMAL":"#C71585","GHOST":"rgb(242, 242, 245)"
	};
					
	// Total size of all segments; we set this later, after loading the data.
	var totalSize = 0; 
	var vis1 = d3.select("#chart").append("svg:svg")
		.attr({
				"width": '100%',
				"height": '100%'
		})
		.attr('viewBox','0 0 '+width+' '+height)
		.attr('preserveAspectRatio','xMinYMin')
		.append("svg:g")
		.attr("id", "burstContainer1")
		.attr("transform", "translate(" + width / 2 + "," + height / 1.9 + ")");
		
	var partition = d3.layout.partition()
		.size([2 * Math.PI, radius * radius])
		.value(function(d) { return d.size; });
	
	var arc1 = d3.svg.arc()
		.startAngle(function(d) { return (d.depth == 2 && indexOfObject(d.parent.children, d.name, "name") == 0) || d.depth == 1 ? d.x+0.01 : d.x; })
		.endAngle(function(d) { return d.x + d.dx; })
		.innerRadius(function(d) { return Math.sqrt(d.y)*1.02; })
		.outerRadius(function(d) { if(d.depth == 2){return Math.sqrt(d.y + d.dy)*1.08;}else{return Math.sqrt(d.y + d.dy);} })
		.cornerRadius(function(d) { return d.depth == 1 ? 10 : 0});
	
	var json = buildHierarchy(data);
	createVisualization(json);
	
	// Main function to draw and set up the visualization, once we have the data.
	function createVisualization(json) {
	
		// Basic setup of page elements:
		// Bounding circle underneath the sunburst, to make it easier to detect
		// when the mouse leaves the parent g.
		vis1.append("svg:circle")
			.attr("r", radius)
			.style("opacity", 0);
		
		// For efficiency, filter nodes to keep only those large enough to see.
		var nodes = partition.nodes(json)
			.filter(function(d) {
			return (d.dx > 0.005); // 0.005 radians = 0.29 degrees
			});
		var group = vis1.data([json]).selectAll("group.group")
						.data(nodes)
						.enter().append("svg:g")
						.on("mouseover", mouseoverFun)
						.on("click",clickFun);
						
		var path = group.append("svg:path")
			.attr("display", function(d) { return d.depth ? null : "none"; })
			.attr("d", arc1)
			.attr("id", "burstPath1")
			.attr("fill-rule", "evenodd")
			.style("fill", function(d,i) { if(d.depth == 1){return pokeTypeColor[d.name]}else if(d.depth == 2){return pokeNameColor[d.name]} })
			.style("cursor", function(d){ return d.depth == 2 ? "pointer" : "auto"; })
			.style("opacity", 1);
			
			
			group.append("text")
				.each(function(d) { d.angle = (d.x + (d.x + d.dx)) / 2; })
				.style("fill","black")
				.attr("dy", ".35em")
				.style("font-size", "12px")
				.style("font-weight", "900")
				.attr("text-anchor", function(d) { return d.angle > Math.PI ? "end" : null; })
				.attr("transform", function(d) {
						return "rotate(" + (d.name == "ICE" ?  d.angle * 180 / Math.PI - 87 :
						d.name == "ELECTRIC" ?  d.angle * 180 / Math.PI - 99  :
						d.name == "FAIRY" ?  d.angle * 180 / Math.PI - 84  : 
						d.name == "GHOST" ?  d.angle * 180 / Math.PI - 82  :
						d.name == "POISON" ?  d.angle * 180 / Math.PI - 97  :
						d.name == "WATER" ||d.name == "POISON" ?  d.angle * 180 / Math.PI - 97.3  :
						d.name == "PSYCHIC" ? d.angle * 180 / Math.PI - 81 :
						d.name == "FIRE" || d.name =="BUG" ?  d.angle * 180 / Math.PI - 94  :
						d.angle > Math.PI ? d.angle * 180 / Math.PI - 80 :
						d.angle * 180 / Math.PI -96) + ")"
				
							+ (d.angle > Math.PI ?"translate(" + (Math.sqrt(d.y) + 35) + ")": "translate(" + (Math.sqrt(d.y) + 52) + ")")
							+ (d.name == "ICEq" ? "translate(" + (Math.sqrt(d.x) + -20) + ")":(d.angle > Math.PI ?"translate(" + (Math.sqrt(d.x) + -5) + ")": "translate(" + (Math.sqrt(d.x) + -20) + ")") )
							+ (d.angle > Math.PI ? "rotate(83)"  : "")
							+ (d.name == "ELECTRIC" ? "rotate(98)" :d.angle < Math.PI ? "rotate(95)" : "");
				})
				.text(function(d) { 
						if(d.depth == 2){return null;}
						else if (d.depth == 1){return d.name;}
						else{return null;}
				})
				.attr("class","key tooltips white")
				.attr("data-container","body");
			
		// Add the mouseleave handler to the bounding circle.
		d3.select("#burstContainer1").on("mouseleave", mouseleaveFun);
		
		// Get total size of the tree = value of root node from partition.
		totalSize = path.node().__data__.value;
		
		 $( document ).ready(function() {   						 		  
		     var element = path[0][0].__data__.children[3].children[5];
		      mouseoverFun(element);
	   });
	};
	
	var cntr = $("#chart"), Cw = cntr.width(), Ch = cntr.height(), pokeNames = [];
	
	function clickFun (d){
		if(d.depth == 2){
			var found = pokeNames.indexOf(d.name) > -1;
			if(!found){
				pokeNames.push(d.name);
				console.log(pokeNames);
			}else{
				pokeNames.splice(pokeNames.indexOf(d.name), 1);
				console.log(pokeNames);
			}
			var result = pokeNames.join(',');
			
			// ajax refresh call  - to update right side charts
			rdAjaxRequestWithFormVars('rdAjaxCommand=RefreshElement&rdRefreshElementID=testRef,rightside_col_1,rightside_col_2,rightside_col_3&PokemonList='+result+'&rdReport=Default','false','',true,null,null,null,true);
		}
	}
	
	// Fade all but the current sequence, and show it in the breadcrumb trail.
	function mouseoverFun(d) {
		var desc, attack, defense, stamina ;
		if(d.depth == 2){
			desc = d.name;
			for (var i = 0; i < data.length; i++) {
				if(data[i].pokeName == desc){
					attack = data[i].attack;
					defense = data[i].defense;
					stamina = data[i].stamina;
					
					var pokemon = d.name.toLowerCase().replace(',','').replace('.','-').replace('\'','').replace('_','-');
					d3.select("#pokemon_img")		
						.attr('src',"https://img.pokemondb.net/artwork/"+pokemon+".jpg");
						
					d3.select("#desc1").text(desc);
					d3.select("#lblAttack").text(attack);
					d3.select("#lblDefense").text(defense);
					d3.select("#lblStamina").text(stamina);
				}
			};	
		}
		var percentage = (100 * d.value / totalSize).toPrecision(3);
		var percentageString = percentage + "%";
		if (percentage < 0.1) {
			percentageString = "< 0.1%";
		}
		
		d3.select("#circusAttr").style("visibility","");
		var sequenceArray = getAncestors(d);
		
		// Fade all the segments.
		if(d.depth == 2){
			vis1.selectAll("#burstPath1").style("opacity", 0.5);
			
			// Then highlight only those that are an ancestor of the current segment.
			vis1.selectAll("#burstPath1")
				.filter(function(node) {
							return (sequenceArray.indexOf(node) >= 0);
						})
				.style("opacity", 1);
		}
	}
	
	// Restore everything to full opacity when moving off the visualization.
	function mouseleaveFun(d) {
	
		// Hide the breadcrumb trail
		d3.select("#trail").style("visibility", "hidden");
		
		// Deactivate all segments during transition.
		d3.selectAll("#burstPath1").on("mouseover", null);
		
		// Transition each segment to full opacity and then reactivate it.
		d3.selectAll("#burstPath1")
			.transition()
			.duration(1)
			.style("opacity", 100)
			.each("end", function() {
					d3.select(this).on("mouseover", mouseoverFun);
					});
		
		d3.select("#circusAttr").style("visibility","hidden");
				
	}
	
	// function to get index of object in an array
	function indexOfObject(myArray, searchTerm, property) {
		for(var i = 0, len = myArray.length; i < len; i++) {
			if (myArray[i][property] === searchTerm) return i;
		}
		return -1;
	}
	
	// Given a node in a partition layout, return an array of all of its ancestor
	// nodes, highest first, but excluding the root.
	function getAncestors(node) {
		var path = [];
		var current = node;
		while (current.parent) {
			path.unshift(current);
			current = current.parent;
		}
		return path;
	}
	
	// Take a 2-column CSV and transform it into a hierarchical structure suitable
	// for a partition layout. The first column is a sequence of step names, from
	// root to leaf, separated by hyphens. The second column is a count of how 
	// often that sequence occurred.
	function buildHierarchy(csv) {
		var root = {"name": "root", "children": []};
		for (var i = 0; i < csv.length; i++) {
			var sequence = csv[i][0];
			var size = +csv[i][1];
			if (isNaN(size)) { // e.g. if this is a header row
				continue;
			}
			var parts = sequence.split("-");
			var currentNode = root;
			for (var j = 0; j < parts.length; j++) {
				var children = currentNode["children"];
				var nodeName = parts[j];
				var childNode;
				if (j + 1 < parts.length) {
					// Not yet at the end of the sequence; move down the tree.
					var foundChild = false;
					for (var k = 0; k < children.length; k++) {
						if (children[k]["name"] == nodeName) {
							childNode = children[k];
							foundChild = true;
							break;
						}
					}
					// If we don't already have a child node for this branch, create it.
					if (!foundChild) {
						childNode = {"name": nodeName, "children": []};
						children.push(childNode);
					}
					currentNode = childNode;
				} else {
					// Reached the end of the sequence; create a leaf node.
					childNode = {"name": nodeName, "size": size};
					children.push(childNode);
				}
			}
		}
			
		return root;
	};
	
	
}