function getFullReportName(){
var url = document.location.href;
if(!url.includes("rdReport="))
return null;
return getParam( "rdReport", url );
}

function getReportFolderName(url){
	if(!url)
	return null;
	if(!url.includes("."))
	return null;
	return url.split(".")[0];
}

function getReportFolders(url){
	if(!url)
	return null;
	if(!url.includes("."))
	return null;
	var temp = new Array();
	temp = url.split(".");
	return temp;
}

function getReportFileName(url){
	if(!url)
	return null;
	if(!url.includes(".")&& url != '')
	// need to return the url (report with no folder)
	return null;
	var result= url.split(".");
	return result[result.length-1];
}

function appendToElementClass(elementID, tag){
	var element = document.getElementById(elementID);
	if(element == null)
	return null;
	element.className+=" " + tag;
	return "ok";
}

function getParam( name, queryString ) {
    name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
    var regexS = "[\\?&]"+name+"=([^&#]*)";
    var regex = new RegExp( regexS );
    var results = regex.exec( queryString );
    return results == null ? null : results[1];
}

function getChartName(type){
	var chart;
	switch (type) { 
		case 'line':
			chart = 'Line Chart';
			break;
		case 'bar':
			chart = 'Bar Chart';
			break;
		case 'area':
			chart = 'Area Chart';
			break;
		case 'bubble':
			chart = 'Bubble Chart';
			break;
		case 'scatter':
			chart = 'Scatter Chart'
			break;
		default:
			chart = 'Chart';
	}
	return chart;
}