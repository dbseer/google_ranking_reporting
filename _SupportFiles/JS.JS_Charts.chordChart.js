drawChords ('chordChart', chordJson);

function drawChords (chartContainerID, data) {
      //*******************************************************************
      //  CREATE MATRIX AND MAP
      //*******************************************************************
        var mpr = chordMpr(data);

        mpr.addValuesToMap('source')
           .setFilter(function (row, a, b) {
            	return (row.source === a.name && row.target === b.name )
           })
           .setAccessor(function (recs, a, b) {
				if (!recs[0]) return 0;
				return +recs[0].size;
           });
        
      //*******************************************************************
      //  DRAW THE CHORD DIAGRAM
      //*******************************************************************
        var matrix = mpr.getMatrix(),  mmap = mpr.getMap();
	    var $container = $('#'+chartContainerID);
			if($('#'+chartContainerID+' #'+chartContainerID+'_tooltip').length <= 0){
				$container.append("<div id='" + chartContainerID + "_tooltip' class='tooltipbox'></div>")
			};
		
        var w = $container.width(), h = $container.height(), r1 = w / 2, r0 = r1 - 115;

        var fill = d3.scale.category20b();

        var chord = d3.layout.chord()
            .padding(.02)
            .sortSubgroups(d3.descending)
            .sortChords(d3.descending);
			
			

        var arc = d3.svg.arc()
            .innerRadius(r0)
            .outerRadius(r0 + 25);

        var chordsvg = d3.select('#'+chartContainerID).append("svg:svg")
            .attr({
      				"width": '100%',
					"height": '100%'
  			})
			.attr('viewBox','0 0 '+w+' '+w/1.1)
            .attr('preserveAspectRatio','xMinYMin meet')
            .append("svg:g")
            .attr("id", "circle")
            .attr("transform", "translate(" + w / 2 + "," + w / 2.2 + ")");
			

            chordsvg.append("circle")
                .attr("r", r0 + 25);

        var rdr = chordRdr(matrix, mmap);
        chord.matrix(matrix);

        var g = chordsvg.selectAll("g.group")
            .data(chord.groups())
            .enter().append("svg:g")
            .attr("class", "group")
            .on("mouseover", chordmouseover)
            .on("mouseout", function (d) { d3.select("#"+chartContainerID+"_tooltip").style("visibility", "hidden") });

        g.append("svg:path")
            .style("fill", function(d) { return fill(d.index); })
            .attr("d", arc);

        g.append("svg:text")
            .each(function(d) { d.angle = (d.startAngle + d.endAngle) / 2; })
            .attr("dy", ".35em")
            .style("font-family", "helvetica, arial, sans-serif")
            .style("font-size", "15px")
			.style("font-weight", "bold")
            .attr("text-anchor", function(d) { return d.angle > Math.PI ? "end" : null; })
            .attr("transform", function(d) {
              return "rotate(" + (d.angle * 180 / Math.PI - 90) + ")"
                  + "translate(" + (r0 + 26) + ")"
                  + (d.angle > Math.PI ? "rotate(180)" : "");
            })
            .text(function(d) { return rdr(d).gname.substr(0,3); });

          var chordPaths = chordsvg.selectAll("path.chord")
                .data(chord.chords())
                .enter().append("svg:path")
                .attr("class", "chord tooltips")
                .style("stroke", function(d) { return d3.rgb(fill(d.source.index)).darker(); })
                .style("fill", function(d) { return fill(d.source.index); })
                .attr("d", d3.svg.chord().radius(r0))
                .on("mouseover", function (d) {
                  d3.select("#"+chartContainerID+"_tooltip")
                    .style("visibility", "visible")
                    .html(chordTip(rdr(d)))
                    .style("top", function () { return (d3.event.clientY)+"px"})
                    .style("left", function () { return (d3.event.clientX-160)+"px";})
                })
                .on("mouseout", function (d) { d3.select("#tooltipbox").style("visibility", "hidden") });

          function chordTip (d) {
            var p = d3.format(".2%")
            return "Flights Info:<br/> (" + d.svalue + ") of "
              + d.sname.substr(0,3) + " flights departing to " + d.tname.substr(0,3)
              + (d.sname.substr(0,3) === d.tname.substr(0,3) ? "": ("<br/>while...<br/> (" + d.tvalue + ") of "
              + d.tname.substr(0,3) + " flights departing to " + d.sname.substr(0,3)))
          }

          function groupTip (d) {
            var p = d3.format(".1%"), q = d3.format(".2r")
            return "Airport :"+d.gname.substr(4,d.gname.length)+"<br/>Avg. Departing flights to <br/>top 20 airports for ("
                + d.gname.substr(0,3) + ") : " + q(d.gvalue) + "<br/>"
          }

          function chordmouseover(d, i, x, y) {
		  	var top ,left ;
			if(x !=null & y!=null){top = y; left = x;}
			else{ top = d3.event.clientY; left = d3.event.clientX-160;}
            d3.select("#"+chartContainerID+"_tooltip")
              .style("visibility", "visible")
              .html(groupTip(rdr(d)))
              .style("top", function () { return (top)+"px"})
              .style("left", function () { return (left)+"px";})

            chordPaths.classed("fade", function(p) {
              return p.source.index != i
                  && p.target.index != i;
            });
          }
		  
		  $( document ).ready(function() {   						 		  
		  		var gData = g[0][0];
  				var element = gData.__data__;
				var index = 0;
				
				chordmouseover(element, index, 350, 175);
		  });
      }

    