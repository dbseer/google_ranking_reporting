if (LogiXML.Ajax.AjaxTarget) {
	//LogiXML.Ajax.AjaxTarget().on('reinitialize', function () { createFilterTags (); });
}

function getItem(el){
	var item = {};
	item.id = el.attr("id");
	item.refresh_elements = '';
	item.filter_name = '';
	var attributes = document.getElementById(item.id);
	item.refresh_elements = getRefreshElements(item.id, attributes, 1)
	// TODO : if no refreshElements then the element doesn't have an event, need to get them from a submit filter btn or so.
	
	// Select List
	if(el.is("select")){
		$("#"+item.id+" option:selected").each(function(){
			item.filter_name += $(this).text()+",";
		})
		item.filter_value = el.val();
		item.filter_name = item.filter_name.substring(item.filter_name.length-1) == ',' ? item.filter_name.substring(0,item.filter_name.length-1) : item.filter_name;
	
	// Inputs	
	}else if(el.is("input")){
		// Input Text
		if(el.attr('type') == 'TEXT'){
			item.filter_value = el.val();
			item.filter_name = el.val();
			
		// Input Checkbox
		}else if(el.attr('type') == 'CHECKBOX'){
			item.filter_value = el.attr('checked') == 'checked' ? el.val() : el.attr('rduncheckedvalue');
			item.filter_name = el.attr('checked') == 'checked' ? 'Yes' : 'No';
			
		// Input Radio
		}else if(el.attr('type') == 'radio'){
			var parent = el.parents('span[type=rdRadioButtonGroup]');
			item.id = parent.attr('name')
			item.filter_name = parent.find('label[for='+$('.filter-tag[type=radio]:checked').attr('id')+']').text();
			item.filter_value = parent.find('.filter-tag[type=radio]:checked').val() == 1 ? parent.find('.filter-tag[type=radio]:checked').val() : '';
		}
		
	// Checkbox List
	}else if(el.attr('data-checkboxlist') == 'True'){
		$("#"+el.attr('id')+" input[type=checkbox]:checked").parent('label').find('span').each(function(){
			item.filter_name = item.filter_name + ',' + $(this).text();
			item.filter_value = item.filter_value + ',' + $(this).parent().find('input[value!=on]:checked').val();
		});
		
	}
	
	return item;
	
}
function createFilterTags () {
	$('.filter-tag').not('label').each(function(i){
		var item = getItem($(this));
		createTag(item.id, item.filter_value, item.filter_name, item.refresh_elements.split(','));
	})
	
}
function getRefreshElements(id, attrs, count){
		//var attrs = document.getElementById(id).attributes;
		var elements = '';
		
		for (var i = 0, n = attrs.length; i < n; i++){
			elements = getParam('rdRefreshElementID',attrs[i].value);  
			if (elements && elements != ''){
				break;
			}
		}
		if((!elements || elements == '') && count == 1){
		
			var arr = [];
			var events = $._data( $("#"+id)[0]).events;
			for (var key in events) {
				if (events.hasOwnProperty(key)) {
					for(var x = 0, y = events[key].length; x < y; x++){
						for (var k in events[key][x]){
							if(events[key][x].hasOwnProperty(k)){
								arr.push({'value':events[key][x][k]})
							}
						}	
					}
				}
			}
			return getRefreshElements(submitID, arr,2);
			
		}else if((!elements || elements == '') && count == 2){
		
			var submitID =  $('.filters-submit').attr('id');
			var attrArr = document.getElementById(submitID).attributes;
			return getRefreshElements(submitID,attrArr,3);
			
		}else{
			return elements;
		}
	}
function createTag(fltID, tagVal, tagCaption, rfshElms){
	rfshElms = rfshElms.join(',');
	var filterTagsContainer = $('.applied-filters');
	if(filterTagsContainer.length <= 0){
		$('body').append("<div id='divAppliedFilters' class='applied-filters'></div>");
	}
	
	var flt_tag = $('div.applied-filters > #'+fltID+"_tag"); 
	if(flt_tag.length == 1){
		if (!tagVal || tagVal=='' || tagVal == '-1'){
			flt_tag.remove();
		}else{
			$('#'+fltID+'_tag #filterCaption').text(fltID+' :'); // TODO : get the Input label(name) instead
			$('#'+fltID+'_tag #filterValue').text(tagCaption);
			$('#'+fltID+'_tag a.close').unbind('click').click(function(){
											clearFilter(fltID.toString(),rfshElms.split(','));
										});
		}
	}else if(flt_tag.length <= 0){
		if (!tagVal || tagVal=='' || tagVal == '-1')
			return;
		else
			flt_tag = document.createElement('span');
			flt_tag.setAttribute('class','filter-item');
			flt_tag.setAttribute('id',fltID+"_tag");
			
			$(flt_tag).append( '<span class="badge" >\
									<i id="filterIcon" class="fa fa-filter"/>\
									<span id="filterCaption" class="bold">'+fltID+' :</span>\
									<span id="filterValue">'+tagCaption+'</span>\
									<a id="clear_'+fltID+'" class="close">\
										<i class="ti ti-close"/>\
									</a>\
								</span>');
			filterTagsContainer.append($(flt_tag));
			document.getElementById("clear_"+fltID).addEventListener('click', function(){
														clearFilter(fltID.toString(),rfshElms.split(','));
													});
	}
	
}

function clearFilter(filterID, rfshElms){
	rfshElms = rfshElms.join(',')+','+filterID;
	filterID = '&'+filterID+'=-1'; // TODO : find a way to get the default value for the filter and pass it here
	var currentReport = getParam('rdReport', window.location.href); //TODO : handle when current rpeort is defult (EX:http://localhost/LogiLibrary/rdPage.aspx)
	rdAjaxRequestWithFormVars('rdAjaxCommand=RefreshElement&rdRefreshElementID='+rfshElms+'&rdReport='+currentReport+filterID,'false','',true,null,null,['','',''],true);
}

function getParam( name, queryString ) {
    name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
    var regexS = "[\\?&]"+name+"=([^&#]*)";
    var regex = new RegExp( regexS );
    var results = regex.exec( queryString );
    return results == null ? null : results[1];
}


function getCaptionFromInput(id, reqVal, type){
	var caption = '';
	var el = $("#"+id);
	// Select List
	if(type == "InputSelectList"){
		$("#"+id+" option:selected").each(function(){
			caption += $(this).text()+",";
		})
		caption = caption.substring(caption.length-1) == ',' ? caption.substring(0,caption.length-1) : caption;
	
	// Inputs	
	}else if(type.substring(0,5) == "Input"){
		// Input Text
		if(type == 'InputText'){
			caption = reqVal;
			
		// Input Checkbox
		}else if(type == 'InputCheckbox'){
			var filter_value = el.attr('checked') == 'checked' ? el.val() : el.attr('rduncheckedvalue');
			caption = el.attr('checked') == 'checked' ? 'Yes' : 'No';
			
		// Input Radio
		}else if(type == 'InputRadioButtons'){
			var inpRadios = $("input[name="+id+"]")
			var parent = inpRadios.parents('span[type=rdRadioButtonGroup]');
			caption = parent.find('label[for='+$('.filter-tag[type=radio]:checked').attr('id')+']').text();
			var filter_value = parent.find('.filter-tag[type=radio]:checked').val() == 1 ? parent.find('.filter-tag[type=radio]:checked').val() : '';
		
		// Checkbox List
		}else if(type == 'InputCheckboxList'){
			$("#"+id+" input[type=checkbox]:checked").parent('label').find('span').each(function(){
				caption = caption + ',' + $(this).text();
				var filter_value = filter_value + ',' + $(this).parent().find('input[value!=on]:checked').val();
			});
		}
	}	
	return caption;
}

