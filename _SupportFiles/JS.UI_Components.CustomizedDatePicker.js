$(document).ready(function(){

	var gIstime='';

	initDatePickers();
	if (LogiXML.Ajax.AjaxTarget) {
		LogiXML.Ajax.AjaxTarget().on('reinitialize', function () { 
		
			initDatePickers(); 
			
			var gIsTimePicker = (gIstime && gIstime == 'true') ? true : false;
			var gdynamicFormat = (gIsTimePicker == true) ? 'L LTS' : 'L'; //dynamic format to support TimePicker
			var USdynamicFormat = (gIsTimePicker == true) ? 'MM/DD/YYYY hh:mm:ss A' : 'MM/DD/YYYY';
					
		});
	}
	
});

function initDatePickers() {

	$('.datePickerRange').each (function (i, el) {
		createDateRange(el);	
		//gIstime=is_time_picker;
	});
	
}

function createDateRange(elm) {
	var elm_id = "#"+$(elm).attr('id'), futureDateRange = false;
	var futureDateRanges = {
			'End Of This Month': [moment(), moment().endOf('month')],
	        'End Of This Quarter': [moment(), moment().endOf('quarter')],
	        'End Of This Year': [moment(), moment().endOf('year')],
	        'Tomorrow': [moment().add(1, 'day'), moment().add(1, 'day')],
	        'Next Month': [moment().add(1, 'month').startOf('month'), moment().add(1, 'month').endOf('month')],
	        'Next Quarter': [moment().add(1, 'quarter').startOf('quarter'), moment().add(1, 'quarter').endOf('quarter')]/*,
	        'Next Year': [moment().add(1, 'year').startOf('year'), moment().add(1, 'year').endOf('year')]*/
	};
	var futureSingleDateRanges = {
			'End Of This Month': [moment().endOf('month'), moment().endOf('month')],
	        'End Of This Quarter': [moment().endOf('quarter'), moment().endOf('quarter')],
	        'End Of This Year': [moment().endOf('year'), moment().endOf('year')],
	        'Tomorrow': [moment().add(1, 'day'), moment().add(1, 'day')],
	        'End Of Next Month': [moment().add(1, 'month').endOf('month'), moment().add(1, 'month').endOf('month')],
	        'End Of Next Quarter': [moment().add(1, 'quarter').endOf('quarter'), moment().add(1, 'quarter').endOf('quarter')]/*,
	        'Next Year': [moment().add(1, 'year').endOf('year'), moment().add(1, 'year').endOf('year')]*/
	};
	var normalDateRanges = {
			'Next Month': [moment().add(1, 'month').startOf('month'), moment().add(1, 'month').endOf('month')],
          	'This Year': [moment().startOf('year'), moment().endOf('year')],
	        'This Quarter': [moment().startOf('quarter'), moment().endOf('quarter')],
	        'This Month': [moment().startOf('month'), moment().endOf('month')],
	        'Today': [moment(), moment()],
	        'Last Year': [moment().subtract(1, 'year').startOf('year'), moment().subtract(1, 'year').endOf('year')],
	        'Last Quarter': [moment().startOf('quarter').add(-1, 'quarters'), moment().endOf('quarter').add(-1, 'quarters')],
	        'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
	        'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
	        'Last 365 Days': [moment().subtract(364, 'days'), moment()],
	        'Last 90 Days': [moment().subtract(89, 'days'), moment()],
	        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
	        'Last 10 Days': [moment().subtract(9, 'days'), moment()]
    };

	var isTimePicker = $(elm).hasClass('timePicker'), 
	    dynamicFormat = (isTimePicker == true) ? 'L LTS' : 'L',
	    USdynamicFormat = (isTimePicker == true) ? 'MM/DD/YYYY hh:mm:ss A' : 'MM/DD/YYYY';
	
	var locale = window.navigator.userLanguage || window.navigator.language;
	moment.locale(locale);
	
	var config = {locale: {cancelLabel: 'Clear'}};
	
	config.opens = 'right';//(App.isRTL() ? 'left' : 'right');
	config.timePicker = isTimePicker;
	config.locale.format = dynamicFormat;
	config.disableHoverDate = false;
	
	var isSingleDate = $(elm).hasClass('single'), 
	    inpStartDate = $(elm_id+" input:hidden"+elm_id+"_Start"),
	    inpEndDate = $(elm_id+" input:hidden"+elm_id+"_End");
	
	if (!isSingleDate && inpStartDate && inpEndDate) {
	
		if (inpStartDate.val() != "") {
			var SD = moment(inpStartDate.val()).format(dynamicFormat);
			inpStartDate.val(moment(SD,dynamicFormat).format(USdynamicFormat));
			config.startDate = SD;
		}
		if (inpEndDate.val() != "") {
			var ED = moment(inpEndDate.val()).format(dynamicFormat);
			inpEndDate.val(moment(ED,dynamicFormat).format(USdynamicFormat));
			config.endDate = ED;
		}
		config.autoUpdateInput = false;
		config.ranges = futureDateRange == true ? futureDateRanges : normalDateRanges;
		
	} else {
	
		isSingleDate = true;
		if (inpEndDate.val() != "") {
			var ED = moment(inpEndDate.val()).format(dynamicFormat);
			inpEndDate.val(moment(ED,dynamicFormat).format(USdynamicFormat));
			config.startDate = ED;
		}
		config.singleDatePicker = true;
		config.autoUpdateInput = true;
		config.ranges = futureDateRange == true ? futureSingleDateRanges : {
			'Next Month': [moment().add(1, 'month').startOf('month'), moment().add(1, 'month').startOf('month')],
          	'This Year': [moment().startOf('year'), moment().startOf('year')],
	        'This Quarter': [moment().startOf('quarter'), moment().startOf('quarter')],
	        'This Month': [moment().startOf('month'), moment().startOf('month')],
	        'Today': [moment(), moment()],
	        'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')]
        };
		
	}
	
	$(elm).daterangepicker(config, function(start, end, label) {
		if (!isSingleDate) {
			inpStartDate.val(moment(start.toString()).format(USdynamicFormat));
			inpEndDate.val(moment(end.toString()).format(USdynamicFormat));
		} else {
			inpEndDate.val(moment(start.toString()).format(USdynamicFormat));
		}
		rdAjaxRequestWithFormVars('rdAjaxCommand=RefreshElement&rdRefreshElementID=exportChartClicks,clicksBChart,exportChartImpr,impressionsHeatMapDiv,clicksHeatMapDiv,imprBChart,keywordBChart,exportChart,col-md-4-1,col-md-4-2,col-md-4-3&rdReport=websiteAnalytics&rdRequestForwarding=Form','false','',true,null,null,['Loading...','rdThemeWaitPanel','rdThemeWaitCaption'],true);
		// javascript:rdAjaxRequestWithFormVars('rdAjaxCommand=RefreshElement&rdRefreshElementID=exportChartClicks,clicksBChart,exportChartImpr,imprBChart,keywordBChart,exportChart,col-md-4-1,col-md-4-2,col-md-4-3&rdReport=websiteAnalytics&rdRequestForwarding=Form','false','',true,null,null,['Loading...','rdThemeWaitPanel','rdThemeWaitCaption'],true);
		//javascript:rdAjaxRequestWithFormVars('rdAjaxCommand=RefreshElement&rdRefreshElementID=row-1,KPIs-row1-2,usertarget-2,employeeHrByClient-2,userbill-2&rdReport=tickUserReporting','false','',true,null,null,null,true);
		//javascript:rdAjaxRequestWithFormVars('rdAjaxCommand=RefreshElement&rdRefreshElementID=KPIs-row1,usertarget,clientbill,employeeHrByClient,userbill,weekbill&rdReport=tickReporting','false','',true,null,null,null,true);
	});
  	
    $(elm).on('apply.daterangepicker', function(ev, picker) {
		if (!isSingleDate) {
			$("input:text", this).val(moment(picker.startDate.toString()).format(dynamicFormat) + ' - ' + moment(picker.endDate.toString()).format(dynamicFormat));
		} else {
			$("input:text", this).val(moment(picker.startDate.toString()).format(dynamicFormat));
			drp.hide();	
		}
		});
  
	$(elm).on('cancel.daterangepicker', function(ev, picker) {
		$("input:text", this).val('');
		if (!isSingleDate) {
			inpStartDate.val('');
			inpEndDate.val('');
		}
	});
	
	var drp = $(elm).data('daterangepicker');
	drp.clickApply();
	if(isSingleDate) {
		//drp.container.find('.range_inputs').hide();
		//drp.container.find('#customRangelbl').hide();
	}
}

/** below converts the date back to en-US before passing it to the query **/
$('#Submit').click(function (){
	
	if (window.navigator.userLanguage !='en-us' || window.navigator.language !='en-us' ){ //only reformat if locale is not en-US
	
		var gIsTimePicker = (gIstime && gIstime == 'true') ? true : false;
		var gdynamicFormat = (gIsTimePicker == true) ? 'L LTS' : 'L'; //dynamic format to support TimePicker
		var USdynamicFormat = (gIsTimePicker == true) ? 'MM/DD/YYYY hh:mm:ss A' : 'MM/DD/YYYY';
		
		$('input[type=hidden]').each(function(i, el){
			if(($(el).attr('id').includes('StartDate') || $(el).attr('id').includes('EndDate')) && $(el).val() != null){
				$(el).val(moment($(el).val().toString()).locale('en-us').format(USdynamicFormat));
			}
		});
	}
});
