var minValue = [];
minValue.push(hcScaterData[0].minValue)
minValue.push(doScaterData[0].minValue)
minValue.push(hcLineData[0].minValue)
minValue.push(doLineData[0].minValue)

var graph = new Rickshaw.Graph( {
	element: document.getElementById("timeSeriesChart"),
	renderer: 'multi',
	stack: false,
	width: 1100,
	height: 500,
	stroke : '50px',
	dotSize: 5,
	padding: {top: 0.02, left: 0.05, right: 0.03, bottom: 0.02},
	min: Math.min(minValue[0], minValue[1], minValue[2], minValue[3]) - 5,
	series: [
	  {
			name: 'Opinion polls- CLINTON',
			data:  hcScaterData,           
			color: 'rgba(42,180,192,2)',
			renderer: 'scatterplot'
			
		}, {
			name: 'Opinion polls- TRUMP',
			data: doScaterData,
			color: 'rgba(226,106,106,2)',
			renderer: 'scatterplot'
			
		}, {
			name: 'Average CLINTON',
			data:  hcLineData,
			renderer: 'line',
			strokeWidth : 4,
			color: 'rgba(42,180,192,1)'
			 
		}, {
			name: 'Average TRUMP',
			data:  doLineData,
			renderer: 'line',
			color: 'rgba(226,106,106,1)',
			strokeWidth : 4
			
		}
	]
	
} );

var slider = new Rickshaw.Graph.RangeSlider.Preview({
	graph: graph,
	element: document.querySelector('#slider')
});

graph.render();

var detail = new Rickshaw.Graph.HoverDetail({
	graph: graph,
	formatter: function(series, x, y, formattedX, formattedY, d) {
		var date = '<span class="date">' + moment(new Date(x)).format("DD MMMM YYYY") + '</span>';
		var swatch = '<span class="detail_swatch" style="background-color: ' + series.color + '"></span>';
		var content = ' ' + series.name + ": " + parseInt(y)+ '%' + ' ' + '<br>' + "Source Name:" + d.value.source + '<br>' + swatch + date;
		var conteentWithoutSource =   ' ' + series.name + ": " + parseInt(y)+ '%' + ' ' + '<br>' + swatch + date;
		if (d.value.source == 'undefined' || d.value.source == null ){
		return 	conteentWithoutSource	
		}else{
		return content;
		}		
	
	}
});

var xAxis = new Rickshaw.Graph.Axis.X({
  graph: graph,
  tickFormat: function(x){
                return moment(new Date(x)).format("MM/DD/YYYY");
              }
});
xAxis.render();

var yAxis = new Rickshaw.Graph.Axis.Y( {
	graph: graph,
	tickFormat: Rickshaw.Fixtures.Number.formatKMBT,
	ticksTreatment: 'glow'
});
yAxis.render();

var legend = new Rickshaw.Graph.Legend({
	graph: graph,
	element: document.querySelector('#legend')
});

var highlighter = new Rickshaw.Graph.Behavior.Series.Highlight({
    graph: graph,
    legend: legend,
    disabledColor: function() { return 'rgba(0, 0, 0, 0.2)' }
});

var highlighter = new Rickshaw.Graph.Behavior.Series.Toggle({
    graph: graph,
    legend: legend
});


var svg = graph.element.querySelector('svg'),
      w = svg.getAttribute('width'),
      h = svg.getAttribute('height');

  svg.removeAttribute('width');
  svg.removeAttribute('height');

  if(w && h) svg.setAttribute('viewBox', [0, 0, parseInt(w)+15, h].join(' '));
  
  
var slider_svg = slider.element.querySelector('svg'),
      slider_w = slider_svg.getAttribute('width'),
      slider_h = slider_svg.getAttribute('height');

  slider_svg.removeAttribute('width');
  slider_svg.removeAttribute('height');

  if(slider_w && slider_h) slider_svg.setAttribute('viewBox', [0, 0, parseInt(slider_w)+35, slider_h].join(' '));

var slider_svg2 = slider.element.querySelector('.rickshaw_range_slider_preview');


		var slider_w2 = slider_svg2.style.width,
      	    slider_h2 = slider_svg2.style.height;
			
			slider_w2 = slider_w2.substring(0, slider_w2.length-2)
			slider_h2 = slider_h2.substring(0, slider_h2.length-2)
			
		
		slider_svg2.style.width = "";
		slider_svg2.style.height = "";
	  
  slider_svg2.removeAttribute('width');
  slider_svg2.removeAttribute('height');

  if(slider_w2 && slider_h2) slider_svg2.setAttribute('viewBox', [0, 0, parseInt(slider_w2)+15, slider_h2].join(' '));
  