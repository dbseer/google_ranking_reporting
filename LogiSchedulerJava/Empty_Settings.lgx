<Setting>
  <RemoteApi Port="56982" PassKey="myKey" />
  <WebRequest Timeout="20" ConcurrencyLimit="10" Stagger="1"  LoggingLevel="WARNING" />
</Setting>

<!--
By default no connection element is required. Java installations default to the embedded Derby database.  DotNet installations default to the embedded VistaDB database.

If networked database is desired then a connection element is necessary. Choose one of the following elements and supply the necessary local information. Place it inside the <Setting></Setting> tag above to activate it. Only the MySql, Oracle and SQL Server databases are currently supported for networked Scheduler access.

Sample MySQL Server Connection:

	<Connection Type="MySql" ID="MySQL" MySqlServer="YourDatabaseServer" MySqlDatabase="YourDatabase"  MySqlUser="YourUser" MySqlPassword="YourPassword" />

Sample Oracle Connection:
	
	<Connection Type="Oracle" ID="Oracle" OracleServer="YourDatabaseServer" OracleDatabase="YourSID"  OracleUser="YourUser" OraclePassword="YourPassword"  />

Sample SQL Server connection:
	
	<Connection Type="SqlServer" ID="SqlServer" SqlServer="YourDatabaseServer" SqlServerDatabase="YourDatabase" SqlServerUser="YourUser" SqlServerPassword="YourPassword" />

RemoteApi:

Port - the IP port for the scheduler service's listener. The Report applications' Connection.Scheduler element must have a matching value. The default is 56982.

PassKey - a password to help secure the scheduler service. The Report applications' Connection.Scheduler element must have a matching value.


WebRequest:

Timeout - the number of minutes in which a requested task must complete before the scheduler gives up.  The default value is 20 minutes.

ConcurrencyLimit - the max number of concurrent requests/tasks. When the limit is reached, additional scheduled tasks wait for one of the other running tasks to complete.  The default value is 5.

Stagger - The number of seconds to wait between running requests/tasks.  The default value is 1 second.

PollingFrequency - the number of seconds that elapse before the database is polled for a task to run. The default is 60 seconds. This should normally not be modified.

LoggingLevel - this controls the logging of events. The allowed values are ERROR, WARNING, INFO, DEBUG or NONE. DotNet installations log events to the LogiXML Event log. Java installations log events to Log/LogiScheduler.log. The default is WARNING. INFO provides a moderate amount of detail about Scheduler events. The DEBUG setting records every web request made.

Version 10 compatibility -  The following element has to be inside the <Setting></Setting> tag above to allow a V10 Info Server application to communicate with V11 Scheduler.
    <Version10Host URL="http://YourHost:8080/YourV10Application" />       
-->

