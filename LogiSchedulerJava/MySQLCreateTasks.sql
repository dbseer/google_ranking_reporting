-- This script creates the Scheduler TASKS table in Oracle MySQL. This is typically done in MySQL Query Browser.

CREATE TABLE Tasks (
`TaskID` int(10) unsigned NOT NULL AUTO_INCREMENT,
`ApplicationID` varchar(50) DEFAULT NULL,
`TaskName` varchar(50) DEFAULT NULL,  
`CustomColumn1` varchar(4000) DEFAULT NULL,  
`CustomColumn2` varchar(4000) DEFAULT NULL,
`IsDisabled` char(1) DEFAULT NULL, 
`ScheduleXml` varchar(4000) DEFAULT NULL,
`ProcessXml` varchar(4000) DEFAULT NULL,
`TimeCreated` datetime DEFAULT NULL,
`TimeModified` datetime DEFAULT NULL,
`TimeLastRun` datetime DEFAULT NULL,
`TimeNextRun` datetime DEFAULT NULL,
`WasSuccessfulLastRun` char(1) DEFAULT NULL,
`TaskResults` varchar(1000) DEFAULT NULL,
`RunAs` varchar(50) DEFAULT NULL,
`IsRunning` char(1) DEFAULT NULL,
`TimeRunning` datetime DEFAULT NULL,
PRIMARY KEY (`TaskID`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1 COLLATE latin1_general_ci;