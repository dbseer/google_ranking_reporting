-- This script creates the Scheduler TASKS table in Microsoft SQL Server. This is typically done in Microsoft SQL Server Management Studio.
-- It is recommended that SQL Server 2008 or later be used due the 8K page size issue with older versions. This script will work but  
-- you may experience a performance degradation if the data in your fields exceeds 8K and the number of tasks is large.  
-- Exceeding 8K triggers behind the scenes row splitting in SQL Server. You need to navigate to the desired database first in your tool.
-- If desired you can substitute your userid for dbo in this script. 

USE [YourDB]
GO

/****** Object:  Table [dbo].[Tasks]    Script Date: 08/31/2012 19:09:52 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Tasks](
	[TaskID] [int] IDENTITY(1,1) NOT NULL,
	[ApplicationID] [nvarchar](20) NULL,
	[TaskName] [nvarchar](50) NULL,
	[CustomColumn1] [nvarchar](4000) NULL,
	[CustomColumn2] [nvarchar](4000) NULL,
	[IsDisabled] [char](1) NULL,
	[ScheduleXml] [nvarchar](4000) NULL,
	[ProcessXml] [nvarchar](4000) NULL,
	[TimeCreated] [datetime] NULL,
	[TimeModified] [datetime] NULL,
	[TimeLastRun] [datetime] NULL,
	[TimeNextRun] [datetime] NULL,
	[WasSuccessfulLastRun] [char](1) NULL,
	[TaskResults] [nvarchar](1000) NULL,
	[RunAs] [nvarchar](50) NULL,
	[IsRunning] [char](1) NULL,
	[TimeRunning] [datetime] NULL
    PRIMARY KEY (TaskID) )
go

