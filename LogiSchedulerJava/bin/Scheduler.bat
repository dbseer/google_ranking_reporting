@echo off
cls
set CURDIR=%~dp0
SET classpathfilelocation=%CURDIR%
SET classpathfile="%classpathfilelocation%rdSchedulerConsole.classpath"
SET debug=-Xdebug -Xnoagent "-Xrunjdwp:transport=dt_socket,address=50202,server=y,suspend=n"

ECHO %CURDIR%rdSchedulerApiServer.jar>%classpathfile%
ECHO %CURDIR%rdSchedulerConsole.jar>>%classpathfile%
ECHO %CURDIR%rdSchedulerDispatcher.jar>>%classpathfile%
ECHO %CURDIR%javaLauncher.jar>>%classpathfile%
ECHO %CURDIR%System.Data.jar>>%classpathfile%
ECHO %CURDIR%derby.jar>>%classpathfile%
ECHO %CURDIR%System.jar>>%classpathfile%
ECHO %CURDIR%System.Xml.jar>>%classpathfile%
ECHO %CURDIR%log4j-1.2.8.jar>>%classpathfile%
ECHO %CURDIR%System.Drawing.jar>>%classpathfile%
ECHO %CURDIR%System.Runtime.Remoting.jar>>%classpathfile%
ECHO %CURDIR%System.Runtime.Serialization.Formatters.Soap.jar>>%classpathfile%
ECHO %CURDIR%System.Web.jar>>%classpathfile%
ECHO %CURDIR%Microsoft.VisualBasic.jar>>%classpathfile%
ECHO %CURDIR%mscorlib.jar>>%classpathfile%
ECHO %CURDIR%log4j-1.2.8.jar>>%classpathfile%
ECHO %CURDIR%System.Deployment.jar>>%classpathfile%
ECHO %CURDIR%Mainsoft.Configuration.jar>>%classpathfile%
ECHO %CURDIR%J2EE.Helpers.jar>>%classpathfile%
ECHO %CURDIR%J2SE.Helpers.jar>>%classpathfile%
ECHO %CURDIR%Mainsoft.Web.jar>>%classpathfile%
ECHO %CURDIR%mysql-connector-java-5.1.23-bin.jar>>%classpathfile%
ECHO %CURDIR%ojdbc6.jar>>%classpathfile%
ECHO %CURDIR%sqljdbc4.jar>>%classpathfile%


for %%i in (%~sf0) do SET launcher="%%~dspijavaLauncher.jar"

REM Launch the application
REM ----------------------
start /B javaw -cp .;%launcher% -Dclasspath.file=%classpathfile% mainsoft.launcher.AppLauncher rdSchedulerConsole.rdSchedulerConsole %*
exit


