package com.logixml;



import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.net.*;
import java.io.*;

import org.jose4j.jwt.JwtClaims;
import org.jose4j.jwt.consumer.JwtConsumer;
import org.jose4j.jwt.consumer.JwtConsumerBuilder;
import org.jose4j.jwt.consumer.JwtContext;

import com.google.gson.Gson;

public class Office365 {

	public static boolean GetJwt(String code, HttpSession session, HttpServletResponse response) {
		try {
			
			String DirectoryTenantID="fff6d0c6-a286-441e-963d-df6ff235ca89";
			
			String url = "https://login.microsoftonline.com/"+DirectoryTenantID+"/oauth2/token";
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			con.setRequestMethod("POST");
			con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
			con.setRequestProperty("charset", "utf-8");
			
			con.setDoInput(true);
			con.setDoOutput(true);
			con.setRequestProperty("Accept", "application/json");
			String ClientId="b42a87dd-e561-4f76-aa53-c34f449ea23c";
			String redirectUri="https://internal.dbseer.com/dbseer-internal/CodeGrant.jsp";
			String resource="api://b42a87dd-e561-4f76-aa53-c34f449ea23c";

			Map<String, Object> params = new LinkedHashMap<String, Object>();
			params.put("grant_type", "authorization_code");
			params.put("client_id", ClientId);
			params.put("code", code);
			params.put("redirect_uri", redirectUri);
			params.put("resource", resource);
			String secret = System.getenv("AADSecretKey");
			System.out.println("Secret is; "+secret);
			params.put("client_secret", secret);



			StringBuilder postData = new StringBuilder();
			for (Entry<String, Object> entry : params.entrySet()) {
				postData.append(entry.getKey());
				postData.append("=");
				postData.append(entry.getValue());
				postData.append("&");
			}

		
			
			byte[] postDataBytes = postData.toString().getBytes("UTF-8");

			con.setRequestProperty("Content-Length", Integer.toString(postData.toString().getBytes().length));


			try {
				OutputStream os = con.getOutputStream();
				os.write(postDataBytes, 0, postDataBytes.length);
			} catch (Exception exWriteStream) {
				System.out.print(exWriteStream);
			}

			try {
				BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "utf-8"));
				StringBuilder responseSt = new StringBuilder();
				String responseLine = null;
				while ((responseLine = br.readLine()) != null) {
					responseSt.append(responseLine.trim());
				}
				
				
				TokenResponse tokenObject = new Gson().fromJson(responseSt.toString(), TokenResponse.class);
				
				return parser(tokenObject.access_token, session, response);
			} catch (Exception exResponse) {
				System.out.print(exResponse);
			}




			return false;

		} catch (Exception ex) {
			System.out.print(ex);
			return false;
		}

	}

	public class TokenResponse {
		private String token_type = null;
		private String scope = null;
		private String expires_in = null;
		private String ext_expires_in = null;
		private String expires_on = null;
		private String not_before = null;
		private String resource = null;
		private String access_token = null;
		private String refresh_token = null;
		private String id_token = null;
	}


//	 
	public static boolean parser(String jwt, HttpSession session, HttpServletResponse response)
			throws UnsupportedEncodingException {
		try {
			
			
			JwtConsumer jwtConsumer = new JwtConsumerBuilder().setDisableRequireSignature().setSkipAllValidators()
					.setSkipSignatureVerification().build();
			
			
			JwtClaims jwtClaims = jwtConsumer.processToClaims(jwt);
			
			String email = (String) jwtClaims.getClaimValue("unique_name");
			
			if (email != null && !email.isEmpty()) {
				
				session.setAttribute("rdUsername", email);
				
				// response.sendRedirect("Default.aspx");
				return true;
			}

			return false;

		} catch (Exception ex) {
			System.out.println(ex);
			return false;
		}

	}

}
